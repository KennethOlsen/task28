﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task28.Model
{
    public class Supervisor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }
    }
}
