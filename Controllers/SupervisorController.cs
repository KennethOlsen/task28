﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task28.Model;

namespace Task28.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }

        // Gets the information of the Supervisors
        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetASupervisor()
        {
            return _context.Supervisors;
        }

        // Adds a Supervisor and returns supervisor that is created at action
        [HttpPost]
        public ActionResult<Supervisor> ConsumeSupervisor(Supervisor supervisor)
        {
            _context.Supervisors.Add(supervisor);
            _context.SaveChanges();

           
            return CreatedAtAction("GetSpecificSupervisorById", new Supervisor { Id = supervisor.Id }, supervisor);
        }

        
        // Gets a Supervisor by Id
        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSpecificSupervisorById(int id)
        {

            var supervisor = _context.Supervisors.Find(id);
            return supervisor;
        }


        // Update a Supervisor by Id
        [HttpPut("{id}")]
        public ActionResult UpdateASupervisor(int id, Supervisor supervisor)
        {

            if (id != supervisor.Id)
            {
                return BadRequest();

            }

            _context.Entry(supervisor).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }


        // Delete a Supervisor by Id
        [HttpDelete("{id}")]
        public ActionResult<Supervisor> DeleteSupervisor(int id)
        {

            var supervisor = _context.Supervisors.Find(id);

            if (supervisor == null)
            {
                return NotFound();
            }

            _context.Supervisors.Remove(supervisor);
            _context.SaveChanges();

            return supervisor;

        }



       




    }
}